---
title: web design inspiration - Parallel Perception
tags: 
author: a
source: https://parallelperception.com/
---
HOME - Parallel Perception  
[![Parallel Perception](https://parallelperception.com/wp-content/uploads/2020/04/Lujan-logo-130x130-1.png)](https://parallelperception.com)  
* [Home](https://parallelperception.com/ "Home")
* [About](https://parallelperception.com/spiral-energetics/ "About")
* [Programs](https://parallelperception.com/programs-summary/ "Programs")
* [Bio](https://parallelperception.com/about/ "Bio")
* [Events](https://parallelperception.com/events/ "Events")
* [Blog](https://parallelperception.com/blog/ "Blog")
* [Books](https://parallelperception.com/books/ "Books")
* [Tuition](https://parallelperception.com/program-registration "Tuition")
* [Resources](https://parallelperception.com/resources/ "Resources")
* [Contact](https://parallelperception.com/email/ "Contact")  
* [](https://www.facebook.com/ParallelPerception/)
* [](https://www.instagram.com/parallelperception/)
* [](https://www.youtube.com/channel/UCIMKDzXUe-Gf849q_aoacMQ)
* Search
*  

###### ABOUT

Enhance Your Personal Power
---------------------------

Lo Ban Pai -- the Art of Spiral Energetics -- is an ancient esoteric system of spiritual development, a highly refined internal practice, that has been hidden for centuries. It consists of a combination of dynamic and meditative movements that promote energy enhancement and assists in the cultivation of the intuitive empath from within.

Lo Ban Pai will help you:  
• Increase your energy  
• Cultivate personal power  
• Awaken heart consciousness  
• Access your third eye capacity  
• Promote optimal health and vitality  
• Improve muscle strength and flexibility  
• Experience heightened states of awareness  
It is a unique coiling system that applies the golden ratio spiral principles of the Tao.  
[LEARN MORE](https://parallelperception.com/shamanic-practices/spiral-energetics "Lujan Matus and the Art of Lo Ban Pai")  
![](https://parallelperception.com/wp-content/uploads/2020/03/candle-circles-feather5.jpg)  

Upcoming Events
---------------

[Oriental ShamanismLo Ban PaiProgramWorkshopsEvents
### Learn Opening the Tao Online
The next online program commences July 2nd.](https://parallelperception.com/2022/05/31/learn-opening-the-tao-online/)  
[Oriental ShamanismLo Ban PaiProgramWorkshopsEvents
### Online Group Class Schedule for 2022
The next program begins March 19th.](https://parallelperception.com/2022/02/21/online-group-class-schedule-for-2022/)  
[Oriental ShamanismLo Ban PaiProgramWorkshopsvideoEvents
### Lo Ban Pai Training in Arizona
Courses available from July onwards.](https://parallelperception.com/2021/06/07/lo-ban-pai-training-in-arizona/)  
[Oriental ShamanismLo Ban PaiProgramEvents
### Online Spiritual Guidance -- Special Group Event in May
Spiritual Guidance program commences May 16th.](https://parallelperception.com/2020/05/04/online-spiritual-guidance-special-group-event-in-may/)  
[All Events](https://parallelperception.com/events/ "Events")  
![](https://parallelperception.com/wp-content/uploads/2020/05/eternal-knot-med3.png)  

###### PROGRAMS

Lo Ban Pai Training Programs
----------------------------

To enable students who attend in block weeks to learn in a traditional way, Lujan has developed standardized programs. This involves teaching certain details ahead of curriculum to lay a foundation that prepares students for future training while ensuring progression along a basic-to-advanced learning path.  
[READ MORE](https://parallelperception.com/shamanic-practices/programs-summary)  

Testimonials
------------

[Oriental ShamanismTestimonials
### Gaze and Yee Shall See!](https://parallelperception.com/2010/09/15/gaze-and-yee-shall-see/)  
[Oriental ShamanismTestimonials
### Personal Power Can Be Obtained Through A Feeling
But which one is the key?](https://parallelperception.com/2017/07/12/personal-power-can-be-obtained-through-a-feeling/)  
[Oriental ShamanismTestimonialsLo Ban PaiProgram
### Dragon's Tears: History and Benefits](https://parallelperception.com/2012/04/05/dragons-tears-history-and-benefits/)  
[Oriental ShamanismTestimonialsLo Ban PaiProgram
### A Journey of Power-- Lo Ban Pai
I realized that my primal self has been...](https://parallelperception.com/2015/09/04/a-journey-of-power/)  
[Lo Ban Pai Testimonials](https://parallelperception.com/tag/testimonials "Testimonials")  

###### BIO

Lujan Matus: The founder of Lo Ban Pai
--------------------------------------

Lujan Matus is a master intuitive empath with a unique ability to immerse individuals and groups in a rarefied state where the subtle currents of the interconnective universe become tangible.  
Lujan's comprehensive teachings provide lasting tools to allow one to recognize and develop their own personal relationship with the unknown.  
[LEARN MORE](http://parallelperception.com/about)  
![](https://parallelperception.com/wp-content/uploads/2020/04/med-buddha-hand2.jpg)  

Latest Blog Posts
-----------------

[Oriental ShamanismTestimonialsLo Ban PaiProgramWorkshopsvideo
### Spiritual Doorways of Perception
How to disappear from yourself](https://parallelperception.com/2022/07/07/spiritual-doorways-of-perception/)  
[Oriental ShamanismTestimonialsLo Ban Pai
### Speaking Truth Opens the Heart
It opens space for love and understanding.](https://parallelperception.com/2022/06/22/speaking-truth-opens-the-heart/)  
[Oriental ShamanismTestimonialsLo Ban PaiProgram
### I Can Feel Energy Inside \& Outside My Body
These movements are extremely effective.](https://parallelperception.com/2022/06/15/i-can-feel-energy-inside-outside-my-body/)  
[Oriental ShamanismTestimonialsLo Ban PaiProgramWorkshopsvideo
### Spiritual Doorways of Perception
How to disappear from yourself](https://parallelperception.com/2022/07/07/spiritual-doorways-of-perception/)  
[All Posts](https://parallelperception.com/blog/ "Blog")  

Book Excerpts
-------------

[Oriental Shamanism](https://parallelperception.com/category/shamanism/)[Book Excerpt](https://parallelperception.com/category/book-excerpt/)[Books](https://parallelperception.com/category/books/)[video](https://parallelperception.com/category/video/)  
[![empathic-consciousness](https://parallelperception.com/wp-content/uploads/2019/08/aquatic-plant-bloom-blooming-732588-2-uai-2046x1364.jpg)](https://parallelperception.com/2019/08/26/an-empathic-perspective-of-consciousness/)  
August 26, 2019

### [An Empathic Perspective Of Consciousness](https://parallelperception.com/2019/08/26/an-empathic-perspective-of-consciousness/)

To discover the true essence of our self  
[Oriental Shamanism](https://parallelperception.com/category/shamanism/)[Book Excerpt](https://parallelperception.com/category/book-excerpt/)  
[![spiritual-shamanic-tarot-workshop](https://parallelperception.com/wp-content/uploads/2019/06/animals-birds-bright-2086748-2-uai-2048x1365.jpg)](https://parallelperception.com/2011/03/16/shamanic-dreaming/)  
March 16, 2011

### [Shamanic Dreaming](https://parallelperception.com/2011/03/16/shamanic-dreaming/)

[Oriental Shamanism](https://parallelperception.com/category/shamanism/)[Book Excerpt](https://parallelperception.com/category/book-excerpt/)  
[![](https://parallelperception.com/wp-content/uploads/2020/01/art-bloom-blooming-blossom-355728-uai-2272x1514.jpg)](https://parallelperception.com/2008/07/23/energetic-assassination/)  
July 23, 2008

### [Energetic Assassination](https://parallelperception.com/2008/07/23/energetic-assassination/)

[All Book Excerpts](https://parallelperception.com/tag/book-excerpt/)  

Highlights
----------

[Oriental ShamanismLo Ban PaiProgramEvents
### Online Spiritual Guidance with Lujan Matus in 2021](https://parallelperception.com/2020/12/02/online-spiritual-guidance-with-lujan-matus-in-2021/)  
[Oriental ShamanismBook Excerptvideo
### Awakening Consciousness](https://parallelperception.com/2019/09/16/awakening-consciousness/)  
[Oriental Shamanismvideo
### Interview with Lujan Matus from the Cosmic Giggle](https://parallelperception.com/2016/02/08/interview-with-lujan-matus-from-the-cosmic-giggle/)  

###### RESOURCES

Books, Audio, Video \& More
---------------------------

As a teacher and guide, Lujan offers tools and techniques that allow you to recognize and develop your own personal relationship with the unknown. He embraces the all-encompassing view of empathy and compassion as the essential foundation of his philosophical approach.  
[LEARN MORE](https://parallelperception.com/resources "Resources")  
![Lujan's Books](https://parallelperception.com/wp-content/uploads/2021/03/books2med.jpg)  
[Oriental Shamanism](https://parallelperception.com/category/shamanism/)[Lo Ban Pai](https://parallelperception.com/category/lo-ban-pai/)[Meditation](https://parallelperception.com/category/meditation/)

### [Mindfulness Meditation](https://parallelperception.com/2019/07/22/mindfulness-meditation/)

A journey towards one's life path viewed from within  

[Read More](https://parallelperception.com/2019/07/22/mindfulness-meditation/)
[](https://parallelperception.com/2019/07/22/mindfulness-meditation/)  

Stay Updated
------------

##### Receive a free 20-minute Healing Meditation MP3 when you join our mailing list.

* Name\*  
  First
* Email\*  

Δ  
[](https://www.facebook.com/ParallelPerception/)  
[](https://www.instagram.com/parallelperception/)  
[](https://www.youtube.com/channel/UCIMKDzXUe-Gf849q_aoacMQ)  
© 2005 -- 2022 Parallel Perception. All rights reserved \| Website by [BMUSED Imaging](https://bmusedimaging.com/) \| [Privacy Policy](https://parallelperception.com/privacy-policy)  
Privacy \& Cookies: This site uses cookies. By continuing to use this website, you agree to their use.   
To find out more, including how to control cookies, see here: [Cookie Policy](https://parallelperception.com/privacy-policy)  
Privacy Preferences  
I Agree  

### Privacy Preference Center

#### Privacy Preferences

Privacy Policy  
Who we are  
Our website address is: https://parallelperception.com  
What personal data we collect and why we collect it  
Comments  
When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor's IP address and browser user agent string to help spam detection.  
An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.  
Media  
If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.  
Contact forms  
Cookies  
If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.  
If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.  
When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select "Remember Me", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.  
Embedded content from other websites  
Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.  
These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.  
Analytics  
How long we retain your data  
If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.  
For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.  
What rights you have over your data  
If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.  
Where we send your data  
Visitor comments may be checked through an automated spam detection service.
[Privacy Policy](https://parallelperception.com/privacy-policy/)  
error: Content is protected !!
